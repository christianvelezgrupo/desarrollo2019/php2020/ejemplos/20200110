<?php
/*creamo una funcion*/
function depurar ($v){
    echo"<pre>";
    var_dump($v);
    echo"</pre>";
}

$vocales=["a","e","i","o","u"];
/*crando un arrays asociativo se hace igual pero de esta forma es más coherente*/
$repeticiones=[
    "a"=>23,
    "e"=>1,
    "i"=>0,
    "o"=>40,
    "u"=>10,      
];
depurar($vocales);
depurar($repeticiones);
$repeticiones["o"]++;
depurar($repeticiones);