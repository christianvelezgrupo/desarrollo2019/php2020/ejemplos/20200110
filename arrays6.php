<?php
$numeros=[
    1,2,3,4,5,6,7,8,9
];
/* imprimir los números del array*/

//for//
for($c=0;$c<count($numeros);$c++){
?>
<li><?= $numeros[$c]?></li>
<?php
}
for($c=0;$c<count($numeros);$c++){
    echo"<li>".$numeros[$c]."</li>";
}

//while//
/* primero hay que inicializar antes del while*/
$c=0;
while($c<count($numeros)){
    ?>
    <li><?= $numeros[$c]?></li>
    <?php
}
/*es lo mismo que el do while*/

//foreach//
    foreach ($numeros as $indice=>$valor){
     echo"<li>$indice:$valor</li>"; 
    }
