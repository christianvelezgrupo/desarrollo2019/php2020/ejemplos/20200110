
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            div{margin: 10px}
        </style>
    </head>
    <body>
        <form action="Ejercicio8.php" method="get">
            <label for="inombre">Nombre del alumno</label>
            <input type="text" id="inombre" name="nombre">
            <div>
                <label for="Inumero1">Numero1</label>
                <input type="number" id="inumero1" name="numero[]">
                <label for="Inumero2">Numero2</label>
                <input type="number" id="inumero2" name="numero[]">    
            </div>
            <div>
                <label for="ipotes">Potes</label>
                <input type="checkbox" id="ipotes" value="potes" name="poblacion[]">
                <label for="isantander">Santander</label>
                <input type="checkbox" id="isantander" value="santander" name="poblacion[]">
            </div>
            <div>
                <label for="irojo">Rojo</label>
                <input type="radio"  id="irojo "value="R" name="color">
                <label for="iazul">Azul</label>
                <input type="radio" id="iazul" value="A" name="color">
            </div>
            <div>
                <label for="inombres">Selecciona Nombres</label> <br>
                <select name="nombres[]" id="inombres" multiple="true">
                    <option value="0">Roberto</option>
                    <option value="1">Silvia</option>
                    <option value="2">Camilo</option>
                </select>
            </div>
            <button>Enviar</button>
        </form>
        <?php
        ?>
    </body>
</html>
